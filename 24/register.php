<?php
if (!empty($_POST['register'])) {
    if (
        !empty($_POST['username']) &&
        !empty($_POST['email']) &&
        !empty($_POST['password']) &&
        !empty($_POST['re_password'])
    ) {
        echo 'user: ' . $_POST['username'] . '<br>';
        echo 'email: ' . $_POST['email'] . '<br>';
        echo 'password: ' . $_POST['password'] . '<br>';
        echo 're password: ' . $_POST['re_password'] . '<br>';
    } else {
        echo 'data not valid';
    }
}
include 'layouts/_header.tpl';
?>
<form method="post">
    <label>
        Username:
        <input type="text" name="username" class="form-control">
    </label>
    <br>
    <label>
        Email:
        <input type="email" name="email" class="form-control">
    </label>
    <br>
    <label>
        Password:
        <input type="password" name="password" class="form-control">
    </label>
    <br>
    <label>
        Re-Password:
        <input type="password" name="re_password" class="form-control">
    </label>
    <button type="submit" name="register" value="register" class="btn btn-success">Register</button>
    <a href="/" class="btn btn-info">login</a>
    <a href="/register.php" class="btn btn-danger">Reset</a>
</form>
<?php
include 'layouts/_footer.php';
?>