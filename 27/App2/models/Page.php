<?php
class Page {
    private $title;
    private $description;
    private $cover;

    public function getTitle()
    {
        return  'Title: ' . $this->title;
    }

    public function getDescription()
    {
        return  'Description: ' . $this->description;
    }

    public function setTitle($title)
    {
        if (is_string($title)) {
            $this->title = $title;
            return $this->title;
        }
        return 'Data Not Valid';
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


}