import React, {Component} from 'react';
import InputCounter from './InputCounter';

export class App extends Component{
    state = {
        counter: 0,
        name: ''
    };

    handleClick = () => {
        let {counter} = this.state;
        counter++;
        this.setState({counter});
        console.log('handleClick')
    };

    handleChange = e => {
        console.log('handleChange');
        this.setState({counter: e.target.value});
    };

    render() {
        console.log('render');
        /*const counter = this.state.counter;
        const name = this.state.name;*/
        const {
            counter,
            name
        } = this.state;
        /*const handleClick = this.handleClick;*/

        return (
            <div>
                Counter: {counter}
                <InputCounter counter={counter}
                              handleChange={this.handleChange}/>
            </div>
        );
    }
}

export default App;