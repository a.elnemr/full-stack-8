import React from 'react';
// props
// props.counter or props.handleChange
const InputCounter = ({counter, handleChange}) => {
    return (
        <input id="counter"
               type="number"
               value={counter}
               onChange={handleChange}/>
    );
};

export default InputCounter;