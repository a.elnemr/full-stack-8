import React from 'react';
import {
    Button,
    Form,
    Container, Row
} from "react-bootstrap";
import List from './Users/List';
import _Form from './Users/Form';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";


class App extends React.Component {

    state = {
        user: {
            username: '',
            email: '',
            id: Math.random()
        },
        users: [
            {
                id: 1,
                username: 'admin',
                email: 'admin@gmail.com'
            },
            {
                id: 2,
                username: 'admin2',
                email: 'admin2@gmail.com'
            }
        ]
    };

    handleChange = e => {
        let user = this.state.user;
        user[e.target.id] = e.target.value;
        this.setState({user});
    };

    handleSubmit = e => {
        e.preventDefault();
        // add user to list
        let {users, user} = this.state;
        // users.push(user);
        users = [...users, user];
        user = {
            username: '',
            email: '',
            id: Math.random()
        };
        this.setState({users, user});

    };

    render() {
        return (
            <Container>

                <Router>
                    <Switch>
                        <Route path="/" exact>
                            <List users={this.state.users}/>
                        </Route>
                        <Route path="/create" exact>
                            <_Form user={this.state.user}
                                   handleChange={this.handleChange}
                                   handleSubmit={this.handleSubmit}/>
                        </Route>
                    </Switch>
                </Router>
            </Container>
        );
    }

}

export default App;