import React from 'react';
import {Button, Form} from "react-bootstrap";
import {Link} from "react-router-dom";

const _Form = ({user, handleChange, handleSubmit}) => {
    return (
        <React.Fragment>
            <Link to="/" className="btn btn-info">
                All Users
            </Link>
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control value={user.email}
                                  onChange={handleChange}
                                  type="email"
                                  placeholder="name@example.com" />
                </Form.Group>
                <Form.Group controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control value={user.username}
                                  onChange={handleChange}
                                  type="text"
                                  placeholder="admin" />
                </Form.Group>
                <Button type="submit" variant="success">Save</Button>
            </Form>
        </React.Fragment>
    );
};

export default _Form;