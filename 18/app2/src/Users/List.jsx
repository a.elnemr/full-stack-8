import React from 'react';
import {Row, Table, Button, Col} from "react-bootstrap";
import {Link} from "react-router-dom";


const List = ({users}) => {
    return (
        <Row>
            <Link to="/create" className="btn btn-success">Create</Link>
            <Col xs={12}>
                <h3>List All Users</h3>
                <Table striped bordered hover style={{width: '100%'}}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {users.map(user => (
                            <tr key={user.id}>
                                <td>{user.id}</td>
                                <td>{user.username}</td>
                                <td>{user.email}</td>
                                <td>
                                    <Button variant="info">Edit</Button>
                                </td>
                            </tr>
                        )
                    )}
                    </tbody>
                </Table>
            </Col>
        </Row>
    );
};

export default List;