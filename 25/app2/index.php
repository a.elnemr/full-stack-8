<?php include_once 'layouts/_header.tpl'; ?>
<?php
# step 1
// catch event
if (isset($_POST['submit'])) {
    $errors = [];
    # step 2
    // validations
    if (empty($_POST['name'])) {
        $errors[] = 'Name is required';
    }

    if (empty($_POST['degree'])) {
        $errors[] = 'Degree is required';
    }

    #step 3
    // logic or business
    if (count($errors) === 0) {
        $result = null;
        $degree = $_POST['degree'];
        if ($degree >= 90) {
            $result = 'A';
        } elseif ($degree >= 80) {
            $result = 'B';
        } elseif ($degree >= 70) {
            $result = 'C';
        } elseif ($degree >= 60) {
            $result = 'D';
        } elseif ($degree >= 50) {
            $result = 'F';
        } else {
            $result = 'FFF';
        }
    }
}
?>

<div class="row">
    <div class="col-12">
        <?php
        for ($i = 0; isset($errors) && $i < count($errors); $i++) :
            ?>
            <div class="alert alert-danger">
                <?= $errors[$i] ?>
            </div>
        <?php
        endfor;
        ?>
    </div>
    <div class="col-12">
        <form method="post">
            <label for="name">Name</label>
            <input type="text" id="name" name="name">
            <hr>
            <label for="degree">Degree</label>
            <input type="number" id="degree" name="degree">

            <div class="text-center">
                <button type="submit"
                        name="submit"
                        class="btn btn-success">Submit
                </button>
            </div>
        </form>
    </div>
    <div class="col-12">
        <?php
        if (isset($result)) :
            ?>
            <div class="alert alert-success">
                <?= $result ?>
            </div>
        <?php
        endif;
        ?>
    </div>

</div>

<?php include_once 'layouts/_footer.php' ?>
