import React from 'react';
import {_NavDesktop as NV} from './components/layouts/_Nav';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Post from './components/posts';

class App extends React.Component {

    render() {
        return (
            <React.Fragment>
                <Router>
                    <NV/>
                    <div className="container mt-5">
                        <Switch>
                            <Route exact path="/post">
                                <Post.list/>
                            </Route>
                            <Route exact path="/post/create" component={Post.create}/>
                            <Route exact path="/post/show/demo" component={Post.list}/>
                            <Route exact path="/post/show/:id" component={Post.show}/>
                        </Switch>
                    </div>
                </Router>
            </React.Fragment>
        );
    }

}

export default App;