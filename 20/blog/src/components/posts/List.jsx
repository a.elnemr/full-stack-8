import React from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

class List extends React.Component {

    state = {
        posts : []
    };

    componentDidMount() {
        this.getAllPosts();
    }

    getAllPosts = () => {
        let obj = this;
        axios({
            method: 'get',
            url: 'https://jsonplaceholder.typicode.com/posts',
            }).then(function (res) {
            obj.setState({posts: res.data})
        });
    };

    render() {
        return (
            <div className="card p-3">
               <div className="row">
                   <div className="col-md-10">
                       <div className="card-title">
                           <h3>All Posts</h3>
                       </div>
                   </div>
                   <div className="col-md-2">
                       <Link to="/post/create" className="btn btn-success">
                           ADD
                       </Link>
                   </div>
               </div>
                <div className="row">
                    <div className="col-12">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.posts.map(post => {
                                return (
                                    <tr key={post.id}>
                                        <td>{post.id}</td>
                                        <td>{post.title}</td>
                                        <td>
                                            <Link to={'/post/show/' + post.id} className="btn btn-info mr-2">
                                                Show
                                            </Link>
                                            <Link to="/post/edit" className="btn btn-warning mr-2">
                                                Edit
                                            </Link>
                                            <Link to="/post/delete" className="btn btn-danger">
                                                Delete
                                            </Link>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

}

export default List;