import React from 'react';
import axios from "axios";

class Create extends React.Component {

    state = {
        post: {
            title: '',
            body: ''
        }
    };

    addNewPost = data => {
        // let obj = this;
        axios({
            method: 'post',
            url: 'https://jsonplaceholder.typicode.com/posts',
            data: data
        }).then(function (res) {
            console.log(res);
        });
    };

    handleChange = e => {
        let post = this.state.post;
        post[e.target.id] = e.target.value;
        this.setState({post});
    };

    handleSubmit = e => {
        e.preventDefault();
        this.addNewPost(this.state.post);
    };

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" onChange={this.handleChange} id="title" value={this.state.post.title}/>
                    <input type="text" onChange={this.handleChange} id="body" value={this.state.post.body}/>
                    <button>Save</button>
                </form>
            </div>
        );
    }

}

export default Create;