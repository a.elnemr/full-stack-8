import React from 'react';
import axios from "axios";

class Show extends React.Component {
    constructor (props) {
        super(props);
        console.log('constructor');
    }

    state = {
        post:{
            title: 'test',
            body: 'text'
        }
    };

    getPost = () => {
        let obj = this;
        axios({
            method: 'get',
            url: 'https://jsonplaceholder.typicode.com/posts/' + this.props.match.params.id,
        }).then(function (res) {
            obj.setState({post: res.data})
        });
        console.log('getPost');
    };

    componentDidMount() {
        console.log('componentDidMount');
        this.getPost();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    render() {
        return (
            <div>
                <h3>{this.state.post.title}</h3>
                <p>{this.state.post.body}</p>
            </div>
        );
    }

}

export default Show;