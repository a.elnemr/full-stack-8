import React from 'react';

const Comment = ({row}) => {

    return (
        <div className="col-md-12">
            <div className="card mb-3">
                <div className="card-body">
                    <h5 className="card-title">{row.author}</h5>
                    {/*<h5 className="card-title">{props.author}</h5>*/}
                    <p className="card-text">
                        {row.body}
                        {/*{props.body}*/}
                    </p>

                </div>
            </div>
        </div>
    );
};

export default Comment;