import React, {Component, Fragment} from 'react';
import Comment from './Comment';

class App extends Component {

    state = {
        comments: [
            {
                id: 1,
                author: 'Ahmed',
                body: "Ahmed quick example text to build on the card title and make up the bulk of the card's content."
            },
            {
                id: 2,
                author: 'Ali',
                body: "Ali quick example text to build on the card title and make up the bulk of the card's content."
            },
            {
                id: 3,
                author: 'Mohamed',
                body: "Mohamed quick example text to build on the card title and make up the bulk of the card's content."
            },
            {
                id: 4,
                author: 'Sara',
                body: "Sara quick example text to build on the card title and make up the bulk of the card's content."
            }
        ]
    };

    render() {
        return (
            <div className="container">
                <h1>Comments</h1>
                <div className="row">
                    {this.state.
                    comments.
                    map(item => <Comment row={item} key={item.id}/>)}
                </div>
            </div>
        );
    }

}

export default App;