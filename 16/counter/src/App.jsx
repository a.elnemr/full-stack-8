import React from 'react';

class App extends React.Component {
    state = {
        counter: 0
    };

    increment = () => {
        let counter = this.state.counter;
        counter++;
        this.setState({counter});
    };

    reset = () => {
        let counter = this.state.counter;
        counter = 0;
        this.setState({counter})
    };

    render() {
        return (
            <React.Fragment>
                <h1>Counter {this.state.counter}</h1>
                <button onClick={this.increment}>+</button>
                <button onClick={this.reset}>Reset</button>
            </React.Fragment>
        );
    }

}

export default App;