import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// const APP = function () {
//     return <h1>Demo</h1>;
// };

ReactDOM.render(<App />, document.getElementById("root"));
