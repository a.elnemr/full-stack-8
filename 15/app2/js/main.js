let list = [
    {
        completed: false,
        title: 'Cras justo odio',
        id: 5
    },
    {
        completed: false,
        title: 'Dapibus ac facilisis in',
        id: 9
    },
    {
        completed: true,
        title: 'Morbi leo risus',
        id: 99
    },
    {
        completed: false,
        title: 'Porta ac consectetur ac',
        id: 1000
    }
];
// $(document).ready(function () { // document ready
//     getAllListAsHTM();
// });
$(function () { // document ready
    getAllListAsHTM();
});

function insertItem(item) {
    $("#table-data tbody").append(`
        <tr>
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.completed}</td>
            <td><button class="btn btn-info btn-edit" item-id="${item.id}">Edit</button></td>
        </tr>
    `);
}

function getAllListAsHTM() {
    // $("#table-data tbody").empty();
    list.map(function (item) {
        insertItem(item)
    });
}

$("body").on('click', '#btn-task', function () {
    const newTask = {
        id: Date.now(),
        title: $('#input-task').val(),
        completed: false
    };
    // console.table(list);
    list = [...list, newTask];// ES 6
    // list.push(newTask); // ES 5
    // console.table(list);
    insertItem(newTask);
    listPage();
});

$('body').on('click', '.btn-edit', function () {
    const id = $(this).attr('item-id');
    const task = list.filter(function (item) {
        if (id == item.id) {
            return item;
        }

    });
    $("#edit-task").val(task[0].title);
    $("#update-item-id").text(task[0].id);
});

$('body').on('click', '#btn-update', function () {
    const updatedTitle = $("#edit-task").val();
    const id = $("#update-item-id").text();
    list = list.filter(function (item) {
        if (id == item.id) {
            item.title = updatedTitle;
        }

        return item;
    });
    $('#table-data tbody').empty();
    getAllListAsHTM();
    listPage();
});

$('body').on('click', '#btn-add-page', function () {
    $("#add-page").show();
    $("#list-page").hide();
    $("#update-page").hide();
});

$('body').on('click', '#btn-list-page', function () {
   listPage();
});

$('body').on('click', '.btn-edit', function () {
    $("#add-page").hide();
    $("#list-page").hide();
    $("#update-page").show();
});

function listPage() {
    $("#add-page").hide();
    $("#update-page").hide();
    $("#list-page").show();
}
