<?php
// config
require_once 'config/config.php';
require_once 'config/dbConnection.php';
// libs

// models
require_once 'models/User.php';
require_once 'models/Course.php';
require_once 'models/Teacher.php';
require_once 'models/Student.php';
// header
require_once 'layout/_header.php';
// content page
// routing using SERVER
if (empty($_SERVER['PATH_INFO'])) {
    require_once 'view/home.php';
} else {
    $page =  VIEW_DIR . $_SERVER['PATH_INFO'] . '.php';
    if (file_exists($page)) {
        require_once $page;
    } else {
        require_once 'view/404.php';
    }
}
// routing using GET
/*if (empty($_GET['page'])) {
    require_once 'home.php';
} else {
    $page = $_GET['page'] . '.php';
    if (file_exists($page)) {
        require_once $page;
    } else {
        require_once '404.php';
    }
}*/
// end content page
require_once 'layout/_footer.php';