<?php
$dbname = 'lms_2020';
$host = 'localhost';
$username = 'root';
$password = '';
$dsn = "mysql:dbname=$dbname;host=$host";

try {
    $dbh = new PDO($dsn, $username, $password);
} catch (PDOException $exception) {
    echo 'Server Down!';
    // log file
    die;
}
