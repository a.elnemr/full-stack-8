<?php
class Teacher extends User
{
    public $id;

    public $name;

    public $description;

    public $email;

    public $phone;

    public function __construct()
    {

    }

    public function insert()
    {
        return 'insert';
    }

    public function find()
    {
        return 'find';
    }

    public function delete()
    {
        return 'delete';
    }

    public function all()
    {
        global $dbh;
        # query
        $sql = "SELECT * FROM `teachers` WHERE 1";
        # prepare query
        $stm = $dbh->prepare($sql);
        # execute query in MySQL
        $stm->execute();
        # get data
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update()
    {
        return 'update';
    }

    public function save()
    {
        return 'save';
    }
}