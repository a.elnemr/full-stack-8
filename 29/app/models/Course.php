<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: elnemr
 * Date: 11/15/2019
 * Time: 4:10 AM
 */

class Course
{
    /**
     * @var Integer
     */
    public $id;

    /**
     * @var String
     */
    public $title;

    /**
     * @var String
     */
    public $cover;

    /**
     * @var String
     */
    public $description;

    /**
     * @var String
     */
    public $shortDescription;

    /**
     * @var Teacher
     */
    public $teacher;

    public function insert()
    {
        return 'insert';
    }

    public function find()
    {
        return 'find';
    }

    public function delete()
    {
        return 'delete';
    }

    public function all()
    {
        return 'all';
    }

    public function update()
    {
        return 'update';
    }

    public function save()
    {
        return 'save';
    }
}