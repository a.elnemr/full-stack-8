<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: elnemr
 * Date: 11/15/2019
 * Time: 4:53 AM
 */

class Student extends User
{
    public $id;

    public $name;

    public $phone;

    public function insert()
    {
        return 'insert';
    }

    public function find()
    {
        return 'find';
    }

    public function delete()
    {
        return 'delete';
    }

    public function all()
    {
        global $dbh;
        # query
        $sql = "SELECT * FROM `students` WHERE 1";
        # prepare query
        $stm = $dbh->prepare($sql);
        # execute query in MySQL
        $stm->execute();
        # get data
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update()
    {
        return 'update';
    }

    public function save()
    {
        return 'save';
    }
}