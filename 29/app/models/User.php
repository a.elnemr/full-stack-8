<?php

class User
{
    public $id;

    public $username;

    public $password;

    public function all()
    {
        global $dbh;
        # query
        $sql = "SELECT * FROM `users` WHERE 1";
        # prepare query
        $stm = $dbh->prepare($sql);
        # execute query in MySQL
        $stm->execute();
        # get data
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function login()
    {
        return 'login';
    }

    public function register()
    {
        return 'register';
    }
}