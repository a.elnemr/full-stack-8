<h1>Courses</h1>
<?php
$teacher = new Teacher();
$teacher->name = 'Ahmed';
$teacher->email = 'ahmed@gmail.com';
$teacher->phone = '01005296753';
$teacher->description = 'Ahmed Elnemr';

$teacher2 = new Teacher();
$teacher2->name = 'Ali';
$teacher2->email = 'ali@gmail.com';
$teacher2->phone = '01005296703';
$teacher2->description = 'Ali Ahmed';

$course = new Course();
$course->title = 'PHP';
$course->description = 'php content';
$course->shortDescription = 'php short Description';
$course->cover = 'php.png';
$course->teacher = $teacher2;

$course2 = new Course();
$course2->title = 'CSS';
$course2->description = 'css content';
$course2->shortDescription = 'css short Description';
$course2->cover = 'css.png';

?>

<div>
    <h4><?= $course->title ?></h4>
    <h5><i><?= $course->teacher->name ?></i></h5>
    <p><?= $course->description ?></p>
</div>


<div>
    <h4><?= $course2->title ?></h4>
    <h5><i><?= $course2->teacher->name ?></i></h5>
    <p><?= $course2->description ?></p>
</div>
