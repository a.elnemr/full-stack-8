import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import MainNav from "../layouts/MainNav";
import List from "./Products/List";

class App extends React.Component{

    render() {
        return (
            <Router>
                <MainNav/>
                <div className="container">
                    <div className="content mt-5">
                        <Switch>
                            <Route path="/products" component={List} />
                            <Route path="/inventory">
                                inventory
                            </Route>
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }

}

export default App;