import React from 'react';

const ProductRow = ({product}) => {
    return (
        <div className="col-md-6">
            <div className="card mb-2">
                <img src="/" className="card-img-top" alt="image"/>
                <div className="card-body">
                    <h5 className="card-title">{product.name}</h5>
                    <p className="card-text">{product.description}</p>
                    <a href="#" className="btn btn-primary">Add To Inventory</a>
                </div>
            </div>
        </div>
    );
};

export default ProductRow;