import React from 'react';
import ProductRow from "./ProductRow";
import axios from "axios";

class List extends React.Component {

    state = {
        products: [
            {
                id: 1,
                name: 'title',
                image: '/',
                description: 'test demo'
            },
            {
                id: 2,
                name: 'title2',
                image: '/',
                description: 'test demo2'
            }
        ],
        posts: []
    };

    /*getAllProducts = () => {
        axios({
            method: 'get',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Content-Type': 'application/json',
                Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA0NDAxOGM1M2QxOTUzMjUxODExMDBkOTZlMDlmMTdiZGYwNjRmODg0ZTU4YjAxYWRiM2Y5MzIyNjExNTQwN2E3MWJjZTUwNDViNmMzMzZhIn0.eyJhdWQiOiIzIiwianRpIjoiMDQ0MDE4YzUzZDE5NTMyNTE4MTEwMGQ5NmUwOWYxN2JkZjA2NGY4ODRlNThiMDFhZGIzZjkzMjI2MTE1NDA3YTcxYmNlNTA0NWI2YzMzNmEiLCJpYXQiOjE1NzAxMTk4OTUsIm5iZiI6MTU3MDExOTg5NSwiZXhwIjoxNjAxNzQyMjk1LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.cs4FxZy7I3QqRAJQjrVGwnTRRQAdcRwk2ci1tJtz4Jbb59-NuSO33SH-MRuE_1L_7swEC7Q2Vfcg-ofbtRN3tt5jkJWhdYxThWNgxVSU5tLDRjCaQeOFJuOeqakND31gsrOme7sIehn9pDHv2qulEX5cV60h60RQFKz3HjTnUcNbkYJUd7q617VdRGHJCmVfV3wsfpBKOv1wsfiz-0uenLUGqQ2uwN-8jdKwkEbg9uiAK0nyELa_pBiB73BfBJtL_MMh--tjQenQqjn0aWFhwPkeCwomPXbk4rOjo3wZ6LYig0Cj52oBoRRZlVPDUtHC7-Zr8Z-ETJoVV9BRqQWlKKDTYuRrjek3J4QuRVlwZ0JznS1u8qulln91SrnHB5jzDAVl6x7eMFUluMw1II9qOOTftx4KHwaPGAQHZ8IhCow0brsLAp4f0s_SYCSk1pwpBTUlMCEXu-p2SkmfMY5mRQECRckrEu0Q-6Ze3fRpvml-4wGCHv_uo-dcf6qYzJ6sh-vWwfIqe4vEjTipWZ5OM2h92UkdAAW3QSY5rKFRbSEOMbcIjLgiXWqW2ZkytqWJe5UafxX42glWtmNjNuDoTef_OHje1jLEmGAvFj2BUiK8f7R1egfcTiRKRVpwCLbKiBwnumXK5yh-enHw05upNMvoFpbW0SKO3p49O6qaULs`
            },
            url: 'https://egnse.odiggo.com/api/vendor/products',
        }).then(res => {
            console.log(res);
        });
    };*/





    componentDidMount = () => {
        this.getAllProducts()
    };

    render() {
        return (
            <div className="row">
                {this.state.products.map(product => <ProductRow key={product.id} product={product}/>)}
            </div>
        );
    }

}

export default List;