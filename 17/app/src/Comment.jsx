import React from 'react';
// show comment functions
export const Comment = ({comment, increment}) => {
    return (
        <div className="col-12" key={comment.id}>
            <div className="card p-3">
                <div className="card-title">
                    <h3>{comment.author}</h3>
                </div>
                <div className="card-body">
                    <p>{comment.body}</p>
                </div>
                <div className="card-footer">
                    <span className="p-3">{comment.likes}</span>
                    <button onClick={() => increment(comment.id)} className="btn btn-success">Like</button>
                </div>
            </div>
            <br/>
        </div>
    );
};

// Add comment functions
export class AddComment extends React.Component{

    state = {
        comment: {
            author: '',
            body: ''
        }
    };

    handleSubmit = (e) => {
        e.preventDefault(); // stop reload
        console.log(this.state.comment);
    };

    handleInputs = (e) => {
        const comment = this.state.comment;
        comment[e.target.id] = e.target.value;
        this.setState({comment});
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="author">Author</label>
                    <input id="author"
                           value={this.state.comment.author}
                           onChange={this.handleInputs} type="text"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="body">Body</label>
                    <input id="body"
                           value={this.state.comment.body}
                           onChange={this.handleInputs}
                           type="text"
                           className="form-control"/>
                </div>
                <div className="text-center">
                    <button className="btn btn-success">ADD</button>
                </div>
            </form>
        )
    }
}