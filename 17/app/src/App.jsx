import React, {Component} from 'react';
import {AddComment, Comment} from "./Comment";

class App extends Component{

    state = {
        comments: [
            {
                id: 1,
                author: 'Ahmed',
                body: 'Text body',
                likes: 0
            },
            {
                id: 2,
                author: 'Ali',
                body: 'Text body',
                likes: 0
            },
            {
                id: 3,
                author: 'Mohamed',
                body: 'Text body',
                likes: 0
            },
            {
                id: 4,
                author: 'Sayed',
                body: 'Text body',
                likes: 9
            }
        ]
    };

    increment = (id) => {
        let comments = this.state.comments.filter(item => {
            if (item.id === id) {
                item.likes++;
            }
            return true;
        });
        this.setState({comments});
    };

    render() {
        return (
            <div className="container">
                <div className="row mt-5">
                    <AddComment/>
                </div>
                <div className="row mt-2">
                    {this.state.comments.map(comment => {
                        return <Comment key={comment.id} comment={comment} increment={this.increment}/>
                    })}
                </div>
            </div>
        );
    }

}

export default App;