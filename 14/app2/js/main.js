let list = [
    {
        completed: false,
        title: 'Cras justo odio',
        id: 5
    },
    {
        completed: false,
        title: 'Dapibus ac facilisis in',
        id: 9
    },
    {
        completed: true,
        title: 'Morbi leo risus',
        id: 99
    },
    {
        completed: false,
        title: 'Porta ac consectetur ac',
        id: 1000
    }
];
// $(document).ready(function () { // document redy
//     getAllListAsHTM();
// });
$(function () { // document ready
    getAllListAsHTM();
});

function getAllListAsHTM() {
    list.map(function (item) {
        $("#table-data tbody").append(`
            <tr>
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.completed}</td>
            </tr>
        `);
    })
}