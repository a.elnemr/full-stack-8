let list = [
    {completed: false, title: 'Cras justo odio', id:5},
    {completed: false, title: 'Dapibus ac facilisis in', id:9},
    {completed: true, title: 'Morbi leo risus', id:99},
    {completed: false, title: 'Porta ac consectetur ac', id:1000}
];

function getListAsHTML(list) {
    let content = "";
    for (let i=0; i<list.length; i++) {
        if (list[i].completed) {
            content += "<li class='list-group-item completed'>" +
                list[i].id + "# " + list[i].title +
                "<button class='btn btn-info float-right btn-done' task-id='" +
                list[i].id +
                "'>DONE</button> </li>";
        } else {
            content += "<li class='list-group-item'>" +
                list[i].id + "# " + list[i].title +
                "<button class='btn btn-info float-right btn-done' task-id='" +
                list[i].id +
                "'>DONE</button> </li>";
        }

    }

    return content;
}

function updateList() {
    $("#task-list").html(getListAsHTML(list));
}

$(function () {
    // get all list
    updateList();

    // add new task
    $("#btn-task").click(function () {
        const newTask = {
            id: Date.now(),
            title: $("#input-task").val(),
            completed: false
        };

        list.push(newTask);
        updateList();
    });

    // completed task
    // $(".btn-done").on("click", function () {
    //
    // });

});

// completed task
$("#task-list").on("click", ".btn-done", function () {
    const completedTask = $(this).attr('task-id');
    list = list.filter(function (task) {
        if (task.id == completedTask) {
            task.completed = true;
        }
        return task
    });
    console.log(list);
    updateList();
});