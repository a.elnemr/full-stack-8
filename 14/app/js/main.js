function getAllPosts() {
    const url = `https://jsonplaceholder.typicode.com/posts`;
    const method = 'GET';
    $.ajax({
        url: url,
        type: method,
        success: function (res) {
            res.map(function (post) {
                $("#task-list").append(`
                    <li class="list-group-item">
                        ${post.title}
                    </li>
                `)
            });
        }
    });
}

$(function () {
   getAllPosts();
});